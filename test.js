const {
    PublicKey,
    Connection,
    Signer,
    Keypair

} = require('@solana/web3.js');
const {
    createMint,
    getOrCreateAssociatedTokenAccount,
    mintTo,
    transfer,
} = require('@solana/spl-token');
const bs58 = require('bs58');

async function main(connection) {



    const senderPubkey = new PublicKey("7pceGYudQeSsf8aZ3y2wyH23k9k4xYcEskKgxJdKcDLg");
    const senderPrivateKey = bs58.decode("4BU3ZSmLa5XhASXM2KZGZyC6KfFzzZDA7dpJcM9aerCfz8hyxMMxsa6TsmEuRjTBcKhFv2UwuAsx5V529kqLFvZi");

    const sender = new Keypair({
        publicKey: senderPubkey.toBytes(),
        secretKey: senderPrivateKey,
    });

    const recipientPubkey = new PublicKey("99TC4uTzXKjwprMVV7iC9HDhtSJvZ7be9pBiSHazRBaQ");
    const recipientPrivatekey = bs58.decode("G6XqkBevMAdHauww3tEmPo6BSu3SDnEjUCKSPwBJDLEmR2ta8S6XgwAWp7rHXjFvSKuWEACE1uHrwcd89RUoc1W");

    const recipient = new Keypair({
        publicKey: recipientPubkey.toBytes(),
        secretKey: recipientPrivatekey,
    });

    const rewardMint = await createMint(
        connection,
        sender, // payer
        sender.publicKey, // mintAuthority
        sender.publicKey, // freezeAuthority
        0 // decimals
    );

    console.log("reward Mint: ", rewardMint);

    // //token account for rewarding user
    const senderTokenAccount = await getOrCreateAssociatedTokenAccount(connection, sender, rewardMint, sender.publicKey);

    const recipientTokenAccount = await getOrCreateAssociatedTokenAccount(connection, sender, rewardMint, recipient.publicKey);

    //mint for seller 3_000_000 tokens
    let signature = await mintTo(
        connection,
        sender,
        rewardMint,
        senderTokenAccount.address,
        sender,
        3000000,
    );
    console.log("signature: ", signature)

    let amount = 3000;
    signature = await transfer(connection, sender, senderTokenAccount.address, recipientTokenAccount.address, sender, amount);
    console.log(`Send ${amount} Pyth token to ${recipientAddress} at transaction: ${signature}`);
}

const connection = new Connection('https://api.devnet.solana.com', 'confirmed');

main(connection)