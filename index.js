const { Keypair, PublicKey, Connection } = require("@solana/web3.js");
const {
  getOrCreateAssociatedTokenAccount,
  transfer
  
} = require('@solana/spl-token');

// Define the private key as a Buffer
const privateKey = Buffer.from([74,237,217,89,161,81,108,163,26,21,118,130,216,21,77,55,170,76,105,37,21,25,66,6,149,218,175,228,76,33,154,156,26,153,145,29,70,72,190,214,78,198,16,84,96,48,242,173,248,184,111,249,184,12,125,11,60,201,125,200,34,136,128,46]);

const connection = new Connection('https://api.devnet.solana.com', 'confirmed');
const sender = Keypair.fromSecretKey(privateKey);

const PYTH_TOKEN_PUBKEY = 'HZ1JovNiVvGrGNiiYvEozEVgZ58xaU3RKwX8eACQBCt3';
const PYTH_DECIMAL = 6;


async function transferPyth(recipientAddress, amount) {
  // Get the sender's public key
  const recipient = new PublicKey(recipientAddress);

  const senderTokenAccount = await getOrCreateAssociatedTokenAccount(connection, sender, PYTH_TOKEN_PUBKEY, sender.publicKey);

  const recipientTokenAccount = await getOrCreateAssociatedTokenAccount(connection, sender, PYTH_TOKEN_PUBKEY, recipient);

  const signature = await transfer(connection, sender, senderTokenAccount.address, recipientTokenAccount.address, sender, amount);
  console.log(`Send ${amount} Pyth token to ${recipientAddress} at transaction: ${signature}`);
}

transferPyth("5RhSPDNYnWyBhSBjjsfhnXf3kv7KgQPCZZHfKxpBkyyu", 1000 * 10 ** PYTH_DECIMAL);